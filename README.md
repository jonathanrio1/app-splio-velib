# Projet Splio - Velib

## Script de lancement

- `npm install` : Installation/mise à jour des dépendances
- `npm run dev` : Lance le serveur Express sur `localhost:3000`
- `npm test` : Exécute les tests

## Structure du projet

- `/front` : Application React
- `server.js` : Point d'entrée du serveur NodeJS (express)

# Diagramme de séquence

![diagramme_sequence](doc/diagramme_sequence.png)

# Fonctionnalités

Récupère le nombre de vélib disponibles dans les stations les plus proches, à partir de l'adresse Splio

# Releases

1.0.0 Version initiale
