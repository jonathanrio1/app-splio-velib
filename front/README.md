# Projet Splio - Velib

## Script de lancement

- `npm install` : Installation/mise à jour des dépendances
- `npm start` : Exécution de l'application. Le navigateur ouvrira un onglet avec l'URL [http://localhost:3000](http://localhost:3000)

## Fonctionnalités :

L'application Front React appel le Back NodeJS pour chercher les stations dans un rayon donné en paramètre et affiche le nombre de vélos disponibles par station.
