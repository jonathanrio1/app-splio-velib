import React, { Component } from "react";
import AppBar from "@material-ui/core/AppBar";
import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import splioLogo from "./splio-logo.png";
import "./app.css";

class App extends Component {
  state = {
    param: "",
    nbStations: 0,
    items: [],
    searched: false
  };

  handleSubmit = async e => {
    if (this.state.param) {
      e.preventDefault();

      //La recherche a été exécuté, on passe le boolean à true
      this.setState({ searched: true });
      //Récupération de la 1ère page des stations
      let body = await this.callApiVelib(0);

      let records = body.data.records;
      let nbItems = records.length;
      let nbStations = body.data.nhits;

      //Prochaine page à partir de la 20ème station
      let pagination = 20;

      //Tant que le nombre de stations récupérées est inférieure au nombre total des stations,
      //on continue d'appeler l'API avec la pagination actualisée par bloc de 20
      while (nbItems < nbStations) {
        let nextBody = await this.callApiVelib(pagination);
        let nextRecords = nextBody.data.records;
        let nextNbItems = nextRecords.length;
        nbItems += nextNbItems;
        records = [...records, ...nextRecords];
        pagination += 20;
      }
      this.setState({ items: records, nbStations: nbStations });
    } else {
      alert("Le rayon est obligatoire");
    }
  };

  handleChange = e => {
    //Si la saisie n'est pas un digit, on enregistre pas le caractère
    const regex = /^[0-9\b]+$/;
    if (e.target.value === "" || regex.test(e.target.value)) {
      this.setState({ param: e.target.value });
    }
    //Nouvelle saisie pour nouvelle recherche, on réinitialise le boolean searched à false
    this.setState({ searched: false });
  };

  /**
   * Méthode pour appeler l'API Velib
   * @param pagination
   */
  async callApiVelib(pagination) {
    const response = await fetch(
      "/api/velib/getAvailablesSplioVelibFromRadius/" + this.state.param,
      {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          pagination: pagination
        }
      }
    );
    let body = await response.json();
    return body;
  }

  render() {
    const stations = this.state.items;
    const noResult =
      this.state.nbStations === 0 && this.state.param && this.state.searched
        ? `Aucune station trouvée dans un rayon de ${this.state.param} mètres`
        : "";

    return (
      <div>
        <AppBar position="static" className="header">
          <div className="headerContainer">
            <div className="logoLeft">
              <img className="logoSplio" src={splioLogo} alt="splio-logo" />
            </div>
            <div className="title">
              <Typography variant="h6">
                Velib' : recherche de vélo à proximité des bureaux
              </Typography>
              <i>27 Boulevard des Italiens, 75002 Paris</i>
            </div>
          </div>
        </AppBar>
        <p>Veuillez indiquer un rayon de recherche (en mètres) *</p>
        <TextField
          id="outlined-basic"
          label="Distance"
          variant="outlined"
          size="small"
          required
          pattern="[0-9]*"
          placeholder="1000"
          value={this.state.param}
          onChange={this.handleChange}
        />
        <form className="form" onSubmit={this.handleSubmit}>
          <Button type="submit" variant="contained" color="primary">
            Recherchez
          </Button>
        </form>
        <p className={this.state.nbStations === 0 ? "hideStationTitle" : ""}>
          Nombre de stations : {this.state.nbStations}{" "}
        </p>
        <p>{noResult}</p>
        {stations.map((station, index) => (
          <Card key={index} className="card" variant="outlined">
            <CardContent>
              <div className="stationInfo">
                <Typography
                  className="titleStation"
                  variant="h6"
                  component="h2"
                >
                  Station {station.fields.name}
                </Typography>
                <Typography className="cardContent" color="textSecondary">
                  Distance : {Math.trunc(station.fields.dist)} m
                </Typography>
              </div>
              <div className="cardContent">
                <p className="bike">
                  Vélos électriques disponibles :{" "}
                  <span
                    className={
                      station.fields.ebike === 0
                        ? "bikeRed"
                        : station.fields.ebike === 1
                        ? "bikeYellow"
                        : "bikeGreen"
                    }
                  >
                    {station.fields.ebike}
                  </span>
                </p>
                <p className="bike">
                  Vélos mécaniques disponibles :{" "}
                  <span
                    className={
                      station.fields.mechanical === 0
                        ? "bikeRed"
                        : station.fields.mechanical === 1
                        ? "bikeYellow"
                        : "bikeGreen"
                    }
                  >
                    {station.fields.mechanical}
                  </span>
                </p>
                <p className="bike">
                  Nombre total vélos disponibles :{" "}
                  <span
                    className={
                      station.fields.numbikesavailable === 0
                        ? "bikeRed"
                        : station.fields.numbikesavailable === 1
                        ? "bikeYellow"
                        : "bikeGreen"
                    }
                  >
                    {station.fields.numbikesavailable}
                  </span>
                  <span> / {station.fields.capacity}</span>
                </p>
                <p className="bike">
                  Retour vélib possible : {station.fields.is_returning}
                </p>
                <Typography className="actualization" color="textSecondary">
                  Actualisation : {station.fields.duedate}
                </Typography>
              </div>
            </CardContent>
          </Card>
        ))}
      </div>
    );
  }
}

export default App;
