const velibService = require("../services/velib/velib");
const axios = require("axios");

jest.mock("axios");

beforeAll(() => {
  process.env.API_VELIB_WITH_PARAMS =
    "https://opendata.paris.fr/api/records/1.0/search/?dataset=velib-disponibilite-en-temps-reel&facet=name&facet=is_installed&facet=is_renting&facet=is_returning&facet=nom_arrondissement_communes";
  process.env.API_VELIB_ROWS_RETURNED = "20";
  process.env.API_VELIB_LOCATION_RADIUS_SPLIO =
    "48.871144646181314%2C+2.3353273251805056";
});

describe("VelibService", () => {
  it("returns the stations nears from Splio", async () => {
    const mockData = {
      data: require("./result.json")
    };
    axios.get.mockResolvedValue(mockData);
    const response = velibService.getAvailablesSplioVelibFromRadius(
      null,
      20,
      500
    );
    expect(response.nhits).toEqual(mockData.nhits);
  });
});
