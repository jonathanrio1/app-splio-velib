const express = require('express')
const bodyParser = require('body-parser')
const services = require('./services')

const app = express()
const port = process.env.PORT || 3000

app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())

services.startServices(app)

app.listen(port, () =>
  console.log(`App listen on port ${port}`)
)
