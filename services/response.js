const sendResponse = (res, data) => {
  return res.send({
    data,
    errors: res.errors,
  })
}

const addError = (res, message, options) => {
  if (!res.errors) res.errors = []
  res.errors.push({ message, options })
}

const clearError = res => {
  res.errors = []
}

module.exports = {
  sendResponse,
  addError,
  clearError,
}
