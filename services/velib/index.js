const velib = require("./velib");
const { sendResponse, addError } = require("../response");

/**
 * Service permettant de récupérer les vélib disponibles à proximité de Splio
 */
module.exports.velibService = app => {
  app.get(
    "/api/velib/getAvailablesSplioVelibFromRadius/:radius",
    (req, res) => {
      const radius = req.params.radius;
      const pagination = req.headers.pagination ? req.headers.pagination : 0;

      if (radius) {
        velib
          .getAvailablesSplioVelibFromRadius(res, pagination, radius)
          .then(data => {
            sendResponse(res, data);
          });
      } else {
        addError(res, "server.fieldRequired", { field: "Rayon" });
        return;
      }
    }
  );
};
