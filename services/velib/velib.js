const axios = require("axios");
const { addError } = require("../response");
const api = {};

/**
 * Récupère le nombre de vélib disponibles dans les stations les plus proches, à partir des bureaux Splio sur un rayon donné
 * @param {*} res Object Result
 */
api.getAvailablesSplioVelibFromRadius = async (res, pagination, radius) => {
  if (radius) {
    let url = process.env.API_VELIB_WITH_PARAMS;
    url += "&rows=" + process.env.API_VELIB_ROWS_RETURNED;
    url += "&start=" + pagination;
    url += "&geofilter.distance=" + process.env.API_VELIB_LOCATION_RADIUS_SPLIO;
    url += "%2C+" + radius;

    const headers = {
      Accept: "application/json",
      ContentType: "application/json"
    };

    try {
      const response = await axios.get(url, {
        timeout: 60000,
        headers
      });

      if (response && response.data) {
        return response.data;
      }
    } catch (error) {
      console.log("Error", "Velib - getAvailablesSplioVelibFromRadius", error);
      addError(res, "server.serviceError", {
        service: "Velib - getAvailablesSplioVelibFromRadius"
      });
      return null;
    }
  } else {
    console.log(
      "Error",
      "Velib - getAvailablesSplioVelibFromRadius",
      "Rayon de recherche manquant"
    );
    addError(res, "server.serviceError", {
      service: "Velib - getAvailablesSplioVelibFromRadius"
    });
    return null;
  }
};

module.exports = api;
