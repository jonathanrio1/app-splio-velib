const express = require('express')
const { addError, sendResponse } = require('./response')

const { velibService } = require('./velib')


module.exports.startServices = app => {

  app.use(express.static('static'))

  velibService(app)

  app.all('/api/*', (req, res) => {
    addError(res, 'server.serviceNotFound')
    sendResponse(res)
  })
}
